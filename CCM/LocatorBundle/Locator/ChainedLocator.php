<?php

namespace CCM\LocatorBundle\Locator;

use \CCM\LocatorBundle\Locator\LocatorInterface;

class ChainedLocator implements LocatorInterface 
{
    
    private $locators;
    
    public function addLocator(LocatorInterface $locator)
    {
        $this->locators[] = $locator;
    }
    
    /**
     * 
     * @param type $query
     * 
     * @return array
     */
    public function searchByKeyword($query) 
    {
        $result = array();
        foreach ($this->locators as $locator) {
            $result = array_merge($result,$locator->searchByKeyword($query));
        }
        return $result;
    }

}


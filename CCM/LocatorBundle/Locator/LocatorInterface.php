<?php

namespace CCM\LocatorBundle\Locator;

/**
 * 
 */
interface LocatorInterface
{
    /**
     * 
     * @param type $query
     * @return array Description
     */
    public function searchByKeyword($query);
}

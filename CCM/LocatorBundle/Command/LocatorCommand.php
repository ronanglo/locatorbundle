<?php

namespace CCM\LocatorBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

class LocatorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setDefinition(array(
            new InputArgument('query', InputArgument::REQUIRED, 'Any string you want')
        ))
        ->setName('ccm:locator')
        ->setDescription('Get all location by a query')
        ->setHelp(' 
The <info>%command.name%</info> gets all adresses from any locator implemented.
  '
   );
    }
          
                
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        //$result = $this->getContainer()->get('ccm_locator.google_place')->searchByKeyword($input->getArgument('query'));
        $results = $this->getContainer()->get('ccm_locator.chained_locator')->searchByKeyword($input->getArgument('query'));
        
        foreach ($results as $result) {
            $output->writeln('<info>'.$result['name'].'</info>');
            $output->writeln('<info>'.$result['adress'].'</info>');
            $output->writeln('<comment>Found by : '.$result['source'].'</comment>');
            $output->writeln('-----------');
        }
    }
                
    
}
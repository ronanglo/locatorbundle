<?php

namespace CCM\LocatorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use CCM\LocatorBundle\DependencyInjection\Compiler\LocatorPass;

class CCMLocatorBundle extends Bundle
{
    
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new LocatorPass());
    }
    
}
